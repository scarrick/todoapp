/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   TodoApp
 *  Class      :   TaskDetailsDialog.java
 *  Author     :   Sean Carrick
 *  Created    :   Feb 21, 2023
 *  Modified   :   Feb 21, 2023
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ============  ===================  =========================================
 *  Feb 21, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.myco.todo;

import com.github.lgooddatepicker.components.DatePicker;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.util.ResourceBundle;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

/**
 *
 * @author Sean Carrick
 */
public class TaskDetailsDialog extends javax.swing.JDialog {

    /**
     * Creates new form TaskDetailsDialog
     */
    public TaskDetailsDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        descriptionLabel = new JLabel();
        priorityLabel = new JLabel();
        descriptionField = new JTextField();
        priorityField = new JSlider();
        dueDateLabel = new JLabel();
        dueDateField = new DatePicker();
        showAlertField = new JCheckBox();
        daysBeforeField = new JSpinner();
        jLabel1 = new JLabel();
        obsLabel = new JLabel();
        jScrollPane1 = new JScrollPane();
        obsField = new JTextArea();
        jSeparator1 = new JSeparator();
        problemLabel = new JLabel();
        removeButton = new JButton();
        cancelButton = new JButton();
        saveButton = new JButton();
        completedField = new JCheckBox();

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        ResourceBundle bundle = ResourceBundle.getBundle("com/myco/todo/Bundle"); // NOI18N
        setTitle(bundle.getString("TaskDetailsDialog.title")); // NOI18N
        setName("Form"); // NOI18N

        descriptionLabel.setDisplayedMnemonic('D');
        descriptionLabel.setLabelFor(descriptionField);
        descriptionLabel.setText(bundle.getString("TaskDetailsDialog.descriptionLabel.text")); // NOI18N
        descriptionLabel.setName("descriptionLabel"); // NOI18N

        priorityLabel.setDisplayedMnemonic('P');
        priorityLabel.setText(bundle.getString("TaskDetailsDialog.priorityLabel.text")); // NOI18N
        priorityLabel.setName("priorityLabel"); // NOI18N

        descriptionField.setText(bundle.getString("TaskDetailsDialog.descriptionField.text")); // NOI18N
        descriptionField.setName("descriptionField"); // NOI18N

        priorityField.setMajorTickSpacing(1);
        priorityField.setMaximum(3);
        priorityField.setMinimum(1);
        priorityField.setPaintLabels(true);
        priorityField.setPaintTicks(true);
        priorityField.setSnapToTicks(true);
        priorityField.setValue(2);
        priorityField.setName("priorityField"); // NOI18N

        dueDateLabel.setDisplayedMnemonic('u');
        dueDateLabel.setText(bundle.getString("TaskDetailsDialog.dueDateLabel.text")); // NOI18N
        dueDateLabel.setName("dueDateLabel"); // NOI18N

        dueDateField.setName("dueDateField"); // NOI18N

        showAlertField.setMnemonic('A');
        showAlertField.setText(bundle.getString("TaskDetailsDialog.showAlertField.text")); // NOI18N
        showAlertField.setName("showAlertField"); // NOI18N

        daysBeforeField.setModel(new SpinnerNumberModel(0, 0, null, 1));
        daysBeforeField.setName("daysBeforeField"); // NOI18N

        jLabel1.setText(bundle.getString("TaskDetailsDialog.jLabel1.text")); // NOI18N
        jLabel1.setName("jLabel1"); // NOI18N

        obsLabel.setDisplayedMnemonic('O');
        obsLabel.setText(bundle.getString("TaskDetailsDialog.obsLabel.text")); // NOI18N
        obsLabel.setName("obsLabel"); // NOI18N

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        obsField.setColumns(20);
        obsField.setRows(5);
        obsField.setName("obsField"); // NOI18N
        jScrollPane1.setViewportView(obsField);

        jSeparator1.setName("jSeparator1"); // NOI18N

        problemLabel.setFont(new Font("Bitstream Vera Sans", 1, 12)); // NOI18N
        problemLabel.setForeground(new Color(204, 0, 0));
        problemLabel.setText(bundle.getString("TaskDetailsDialog.problemLabel.text")); // NOI18N
        problemLabel.setMaximumSize(new Dimension(23456789, 15));
        problemLabel.setMinimumSize(new Dimension(0, 15));
        problemLabel.setName("problemLabel"); // NOI18N
        problemLabel.setPreferredSize(new Dimension(0, 15));

        removeButton.setIcon(new ImageIcon(getClass().getResource("/com/myco/todo/icons/sm/delete.png"))); // NOI18N
        removeButton.setMnemonic('R');
        removeButton.setText(bundle.getString("TaskDetailsDialog.removeButton.text")); // NOI18N
        removeButton.setName("removeButton"); // NOI18N

        cancelButton.setIcon(new ImageIcon(getClass().getResource("/com/myco/todo/icons/sm/cancel.png"))); // NOI18N
        cancelButton.setMnemonic('n');
        cancelButton.setText(bundle.getString("TaskDetailsDialog.cancelButton.text")); // NOI18N
        cancelButton.setName("cancelButton"); // NOI18N

        saveButton.setIcon(new ImageIcon(getClass().getResource("/com/myco/todo/icons/sm/accept.png"))); // NOI18N
        saveButton.setMnemonic('S');
        saveButton.setText(bundle.getString("TaskDetailsDialog.saveButton.text")); // NOI18N
        saveButton.setName("saveButton"); // NOI18N

        completedField.setMnemonic('C');
        completedField.setText(bundle.getString("TaskDetailsDialog.completedField.text")); // NOI18N
        completedField.setName("completedField"); // NOI18N

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1, GroupLayout.Alignment.TRAILING)
            .addComponent(problemLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                            .addComponent(obsLabel)
                            .addComponent(dueDateLabel)
                            .addComponent(descriptionLabel)
                            .addComponent(priorityLabel))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(descriptionField, GroupLayout.PREFERRED_SIZE, 342, GroupLayout.PREFERRED_SIZE)
                            .addComponent(priorityField, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(dueDateField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(showAlertField)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(daysBeforeField, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel1))
                            .addComponent(jScrollPane1)))
                    .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(completedField)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(saveButton)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cancelButton, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(removeButton, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        layout.linkSize(SwingConstants.HORIZONTAL, new Component[] {cancelButton, removeButton, saveButton});

        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(problemLabel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(descriptionLabel)
                    .addComponent(descriptionField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                    .addComponent(priorityLabel)
                    .addComponent(priorityField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(dueDateLabel)
                    .addComponent(dueDateField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(showAlertField)
                    .addComponent(daysBeforeField, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(obsLabel)
                    .addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(removeButton)
                    .addComponent(cancelButton)
                    .addComponent(saveButton)
                    .addComponent(completedField))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JButton cancelButton;
    private JCheckBox completedField;
    private JSpinner daysBeforeField;
    private JTextField descriptionField;
    private JLabel descriptionLabel;
    private DatePicker dueDateField;
    private JLabel dueDateLabel;
    private JLabel jLabel1;
    private JScrollPane jScrollPane1;
    private JSeparator jSeparator1;
    private JTextArea obsField;
    private JLabel obsLabel;
    private JSlider priorityField;
    private JLabel priorityLabel;
    private JLabel problemLabel;
    private JButton removeButton;
    private JButton saveButton;
    private JCheckBox showAlertField;
    // End of variables declaration//GEN-END:variables
}
