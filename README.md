# TodoApp
This is a 3-step iterative application development project for the *Naturally Java* programming series. This project is available for those who sign up for the course to download as reference for what they are doing, as accompaniment for the course topic.


## Step 1 - Static Prototype
This Step's project is used to introduce the student to the NetBeans IDE and its *Matisse GUI Builder* graphical user interface editor. At the end of this part of the course, the student will have created the entirety of the UI for the ToDo Application, and only have written enough Java code to show the main window and the secondary dialog. All data in this Step is hard-coded and cannot be modified in any way. 

*This step is the V - View portion of the MVC design pattern*.

## Step 2 - Dynamic Prototype
This Step's project picks up where the Step 1 project left off. In this step, the student gets to write the required Java code to create the wiring and plumbing for the ToDo Application in order to make the UI completely work. The JavaBeans class for the data model will also be created in this step, as well as the skeleton of the DAO class, so that it can provide a dynamic prototype of the data service, though without persistent storage.

*This step is the C - Controller portion of the MVC design pattern*.

## Step 3 - Beta Prototype
This Step's prject picks up where Step 2 left off. During this step, the student is introduced to programming against a SQL database and creates all the code that persists the data from the ToDo Application. Along with doing this, little bugs in the UI code are worked out, so that when this step is completed, the student is left with a completed *Beta* release version of the ToDo Application.

*This step is the M - Model portion of the MVC design pattern*.

## Follow-On
Once the Step 3 - Beta Prototype project is completed, we will discuss the hassles and short-comings of this application's design. We will talk about things that would have been nice to have, or executed in a better, cleaner way. We will also talk about how we could improve this application, with an eye to the next series in the *Naturally Java* course.
