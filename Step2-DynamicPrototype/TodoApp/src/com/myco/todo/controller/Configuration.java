/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   TodoApp
 *  Class      :   Configuration.java
 *  Author     :   Sean Carrick
 *  Created    :   Feb 22, 2023
 *  Modified   :   Feb 22, 2023
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ============  ===================  =========================================
 *  Feb 22, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.myco.todo.controller;

import com.myco.todo.model.DatabaseException;
import com.myco.todo.model.Parameters;
import com.myco.todo.model.TaskManager;
import com.myco.todo.view.TasksWindow;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class Configuration implements ActionListener {

    private TasksWindow view;
    private TaskManager model;
    private Parameters params;

    public Configuration(TasksWindow view, TaskManager model, Parameters params) {
        this.view = view;
        this.model = model;
        this.params = params;
        view.addActionListener(this);
        view.addWindowListener(closeDatabase);
    }

    private final WindowListener closeDatabase = new WindowAdapter() {
        @Override
        public void windowClosed(WindowEvent e) {
            model.disconnect();
        }

        @Override
        public void windowClosing(WindowEvent e) {
            model.disconnect();
        }
    };

    @Override

    public void actionPerformed(ActionEvent e) {
        view.setCursor(new Cursor(Cursor.WAIT_CURSOR));

        try {
            if (e.getActionCommand().equals("createTaskList")) {
                newTaskList();
            } else if (e.getActionCommand().equals("openTaskList")) {
                openTaskList();
            } else if (e.getActionCommand().equals("about")) {
                showAbout();
            } else if (e.getActionCommand().equals("exit")) {
                exit();
            }
        } catch (Exception ex) {
            view.setStatus(ex.getMessage(), true);
        }

        view.setCursor(null);
    }

    private final FileFilter hsqlDatabases = new FileFilter() {
        @Override
        public String getDescription() {
            return "Task Lists - HSQLDB Database (*.script)";
        }

        @Override
        public boolean accept(File f) {
            if (f.isDirectory()) {
                return true;
            }
            return f.getName().endsWith(".script");
        }
    };

    private String createOpenDatabase(File databaseFile) throws DatabaseException {
        String filename = databaseFile.getAbsolutePath();
        if (filename.startsWith("file:")) {
            filename = filename.substring(5);
        }
        if (filename.endsWith(".script")) {
            filename = filename.substring(0, filename.length() - 7);
        }
        model.reconnect(filename);
        view.setTaskList(model.listAllTasks(true));
        return filename;
    }

    public void openTaskList(String db) {
        String filename = "";
        try {
            filename = createOpenDatabase(new File(db));
            view.setStatus("Opened task list: " + filename, false);
        } catch (DatabaseException ex) {
            view.setStatus("Cannot open task list : " + db, true);
        }
    }

    public void newTaskList() {
        JFileChooser dlg = new JFileChooser();
        dlg.setDialogTitle("New Task List");
        dlg.setFileFilter(hsqlDatabases);
        File dir = new File(params.getDatabase()).getParentFile();
        dlg.setCurrentDirectory(dir);
        if (dlg.showSaveDialog(view) == JFileChooser.APPROVE_OPTION) {
            try {
                String arg = createOpenDatabase(dlg.getSelectedFile());
                view.setStatus("Task list created: " + arg, false);
            } catch (DatabaseException ex) {
                view.setStatus("Cannot create task list.", true);
            }
        }
    }

    private void openTaskList() {
        JFileChooser dlg = new JFileChooser();
        dlg.setDialogTitle("Open Task List");
        dlg.setFileFilter(hsqlDatabases);
        File dir = new File(params.getDatabase()).getParentFile();
        dlg.setCurrentDirectory(dir);
        if (dlg.showOpenDialog(view) == JFileChooser.APPROVE_OPTION) {
            try {
                String arg = createOpenDatabase(dlg.getSelectedFile());
                view.setStatus("Task list opened: " + arg, false);
            } catch (DatabaseException ex) {
                view.setStatus("Cannot open task list.", true);
            }
        }
    }

    public void showAbout() {
        JOptionPane.showMessageDialog(
                view,
                "ToDo Task Management System\nVersion 1.0\n\nA MyCo Product",
                "About ToDo",
                JOptionPane.INFORMATION_MESSAGE
        );
    }

    private void exit() {
        Runtime.getRuntime().exit(0);
    }

}
