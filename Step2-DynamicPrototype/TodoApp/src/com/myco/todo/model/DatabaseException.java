/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   TodoApp
 *  Class      :   DatabaseException.java
 *  Author     :   Sean Carrick
 *  Created    :   Feb 22, 2023
 *  Modified   :   Feb 22, 2023
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ============  ===================  =========================================
 *  Feb 22, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.myco.todo.model;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class DatabaseException extends ModelException {

    private static final long serialVersionUID = 1L;

    private final String causedBy;

    /**
     * Creates a new instance of <code>DatabaseException</code> without detail
     * message.
     */
    public DatabaseException() {
        this("[No message provided]");
    }

    /**
     * Constructs an instance of <code>DatabaseException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public DatabaseException(String msg) {
        this(msg, "[No cause provided]");
    }

    public DatabaseException(String msg, String causedBy) {
        super(msg);
        this.causedBy = causedBy;
    }

    public DatabaseException(String msg, String causedBy, Throwable cause) {
        super(msg, cause);
        this.causedBy = causedBy;
    }

    public String getCausedBy() {
        return causedBy;
    }

}
