/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   TodoApp
 *  Class      :   TaskManager.java
 *  Author     :   Sean Carrick
 *  Created    :   Feb 22, 2023
 *  Modified   :   Feb 22, 2023
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ============  ===================  =========================================
 *  Feb 22, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.myco.todo.model;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class TaskManager {

    private final Parameters params;
    private final List<Task> tasks;

    public TaskManager(Parameters params) {
        this.params = params;
        tasks = new ArrayList<>();
        Task t1 = new Task();
        t1.setAlert(true);
        t1.setDaysBefore(2);
        t1.setPriority(2);
        t1.setDescription("First task");
        t1.setDueDate(LocalDate.of(2023, Month.MARCH, 1));
        tasks.add(t1);
        Task t2 = new Task();
        t2.setAlert(false);
        t2.setCompleted(true);
        t2.setDescription("First completed task");
        tasks.add(t2);
        Task t3 = new Task();
        t3.setAlert(false);
        t3.setCompleted(false);
        t3.setDescription("First late task");
        t3.setDueDate(LocalDate.of(2023, Month.FEBRUARY, 12));
        tasks.add(t3);
        Task t4 = new Task();
        t4.setAlert(false);
        t4.setDescription("Task with no due date");
        tasks.add(t4);

    }

    public void disconnect() {
        // TODO: disconnect from the database.
    }

    public void reconnect(String database) throws DatabaseException {
        disconnect();
        params.setDatabase(database);
        connect();
    }

    private void connect() throws DatabaseException {
        // TODO: connect to the database.
    }

    public List<Task> listAllTasks() {
        return Collections.unmodifiableList(tasks);
    }

    public List<Task> listAllTasks(boolean priorityOrDate) {
        return Collections.unmodifiableList(tasks);
    }

    public void addTask(Task task) throws ValidationException, DatabaseException {
        validate(task);
        String msg = "Adding " + task.getDescription();
        modify(msg, task);
    }

    public void updateTask(Task task) throws ValidationException, DatabaseException {
        validate(task);
        String msg = "Updating Task with id = " + task.getId();
        modify(msg, task);
    }

    private void validate(Task task) throws ValidationException {
        if (task.getDescription().trim().isEmpty()) {
            throw new ValidationException("A task description must be specified.");
        }
    }

    private void modify(String msg, Task task) {
        if (msg.startsWith("Adding")) {
            tasks.add(task);
        } else {
            for (Task t : tasks) {
                if (t.getId() == task.getId()) {
                    int idx = tasks.indexOf(t);
                    tasks.remove(idx);
                    tasks.add(idx, task);
                    break;
                }
            }
        }
    }

    public void markAsCompleted(int id, boolean completed) throws DatabaseException {
        for (Task task : tasks) {
            if (task.getId() == id) {
                task.setCompleted(completed);
            }
        }
    }

    public void removeTask(int id) {
        for (Task task : tasks) {
            if (task.getId() == id) {
                int idx = tasks.indexOf(task);
                tasks.remove(idx);
            }
        }
    }

    public List<Task> listTasksWithAlert() throws DatabaseException {
        List<Task> alerts = new ArrayList<>();
        for (Task task : tasks) {
            if (task.hasAlert()) {
                alerts.add(task);
            }
        }

        return Collections.unmodifiableList(alerts);
    }

}
