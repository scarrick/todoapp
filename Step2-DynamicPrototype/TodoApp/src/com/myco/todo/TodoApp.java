/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   TodoApp
 *  Class      :   TodoApp.java
 *  Author     :   Sean Carrick
 *  Created    :   Feb 21, 2023
 *  Modified   :   Feb 21, 2023
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ============  ===================  =========================================
 *  Feb 21, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.myco.todo;

import com.myco.todo.controller.Configuration;
import com.myco.todo.controller.QueryEditTasks;
import com.myco.todo.model.Parameters;
import com.myco.todo.model.TaskManager;
import com.myco.todo.view.TasksWindow;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 0.1.0
 * @since 0.1.0
 */
public class TodoApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if (args.length > 1) {
            displayHelp();
        }

        try {
            UIManager.setLookAndFeel("com.formdev.flatlaf.FlatLightLaf");
        } catch (ClassNotFoundException
                | IllegalAccessException
                | InstantiationException
                | UnsupportedLookAndFeelException ex) {
            System.err.println("[WARNING] Unable to install FlatLightLaf");
        }

        Runnable start = () -> {
            doStartApplication(args);
        };
        SwingUtilities.invokeLater(start);
    }

    private static void doStartApplication(String... args) {
        Parameters params = new Parameters();
        TaskManager model = new TaskManager(params);
        TasksWindow view = new TasksWindow(model.listAllTasks());
        QueryEditTasks tasksController = new QueryEditTasks(view, model);
        Configuration configurationController = new Configuration(view, model, params);
        if (args.length == 1) {
            configurationController.openTaskList(args[0]);
        }
        view.setVisible(true);
        tasksController.showAlerts();
    }

    private static void displayHelp() {
        System.out.println("ToDo Task Management System v. 1.0");
        System.out.println("");
        System.out.println("USAGE: java com.myco.todo.TodoApp [*.script file]");
        Runtime.getRuntime().exit(1);
    }

}
