/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   TodoApp
 *  Class      :   TaskCellRenderer.java
 *  Author     :   Sean Carrick
 *  Created    :   Feb 22, 2023
 *  Modified   :   Feb 22, 2023
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ============  ===================  =========================================
 *  Feb 22, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.myco.todo.view;

import com.myco.todo.model.Task;
import java.awt.Color;
import java.awt.Component;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableModel;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class TaskCellRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 4140832439914333393L;

    private final Color completed = new Color(230, 230, 255);
    private final Color alert = new Color(255, 255, 230);
    private final Color late = new Color(255, 230, 230);

    public TaskCellRenderer() {
        super();
    }

    private Color colorizeTask(Task task) {
        if (task.isCompleted()) {
            return completed;
        } else {
            if (task.getDueDate() == null) {
                return Color.white;
            } else if (task.isLate()) {
                return late;
            } else if (task.hasAlert()) {
                return alert;
            } else {
                return Color.white;
            }
        }
    }

    private Object format(Object value) {
        if (value instanceof LocalDate) {
            DateTimeFormatter fmt = DateTimeFormatter.ISO_LOCAL_DATE;
            return fmt.format((LocalDate) value);
        } else if (value instanceof Boolean) {
            return (boolean) value ? "Y" : "N";
        }
        return value;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
            boolean isSelected, boolean hasFocus, int row, int column) {
        value = format(value);
        JLabel label = (JLabel) super.getTableCellRendererComponent(table, value,
                isSelected, hasFocus, row, column);
        if (column != 1) {
            label.setHorizontalAlignment(JLabel.CENTER);
        }
        TableModel tm = table.getModel();
        Task task = ((TasksTableModel) tm).getSelectedTask(row);
        if (isSelected) {
            label.setBackground(Color.gray);
            label.setForeground(colorizeTask(task));
        } else {
            label.setForeground(UIManager.getColor("TextField.text"));
            label.setBackground(colorizeTask(task));
        }
        return label;
    }

}
