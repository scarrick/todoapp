/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   TodoApp
 *  Class      :   TaskDetailsDialog.java
 *  Author     :   Sean Carrick
 *  Created    :   Feb 21, 2023
 *  Modified   :   Feb 21, 2023
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ============  ===================  =========================================
 *  Feb 21, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.myco.todo.view;

import com.github.lgooddatepicker.components.DatePicker;
import com.myco.todo.model.Task;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.URL;
import java.util.ResourceBundle;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.WindowConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author Sean Carrick
 */
public class TaskDetailsDialog extends javax.swing.JDialog {

    private static final long serialVersionUID = 7002373390783960059L;

    private final ActionSupport actionSupport = new ActionSupport(this);
    private final Timer documentTimer;

    private Task task;
    private Task original;
    private boolean newTask;
    private boolean cancelled = false;
    private boolean loading = false;

    /**
     * Creates new form TaskDetailsDialog
     */
    public TaskDetailsDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        loading = true;
        initComponents();

        documentTimer = new Timer(500, (ActionEvent evt) -> {
            if (descriptionField.getText().trim().isEmpty()) {
                setProblem("Description is required", false);
            } else {
                setProblem(null, false);
            }
        });
        documentTimer.setRepeats(true);
        documentTimer.start();
        loading = false;
    }

    public void addActionListener(ActionListener listener) {
        actionSupport.addActionListener(listener);
    }

    public void removeActionListener(ActionListener listener) {
        actionSupport.removeActionListener(listener);
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setNewTask(boolean newTask) {
        this.newTask = newTask;
    }

    public boolean isNewTask() {
        return newTask;
    }

    public void setTask(Task task) {
//        loading = true;
        this.task = task;
        original = task.getClone();
        descriptionField.setText(task.getDescription());
        priorityField.setValue(task.getPriority());
        completedField.setSelected(task.isCompleted());
        dueDateField.setDate(task.getDueDate());
        showAlertField.setSelected(task.hasAlert());
        daysBeforeField.setValue(task.getDaysBefore());
        obsField.setText(task.getObs());
//        loading = false;
    }

    public Task getTask() {
        task.setAlert(showAlertField.isSelected());
        task.setCompleted(completedField.isSelected());
        task.setDaysBefore((int) daysBeforeField.getValue());
        task.setDescription(descriptionField.getText());
        task.setDueDate(dueDateField.getDate());
        task.setObs(obsField.getText());
        task.setPriority(priorityField.getValue());
        return task;
    }

    private void setProblem(String problem, boolean optional) {
        if (optional) {
            problemLabel.setForeground(Color.blue);
        } else {
            problemLabel.setForeground(Color.red);
        }
        if (problem == null) {
            problemLabel.setText("");
            return;
        }
        problemLabel.setText(problem);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        descriptionLabel = new JLabel();
        priorityLabel = new JLabel();
        descriptionField = new JTextField();
        priorityField = new JSlider();
        dueDateLabel = new JLabel();
        dueDateField = new DatePicker();
        showAlertField = new JCheckBox();
        daysBeforeField = new JSpinner();
        daysBeforeLabel = new JLabel();
        obsLabel = new JLabel();
        jScrollPane1 = new JScrollPane();
        obsField = new JTextArea();
        jSeparator1 = new JSeparator();
        problemLabel = new JLabel();
        removeButton = new JButton();
        cancelButton = new JButton();
        saveButton = new JButton();
        completedField = new JCheckBox();

        FormListener formListener = new FormListener();

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        ResourceBundle bundle = ResourceBundle.getBundle("com/myco/todo/Bundle"); // NOI18N
        setTitle(bundle.getString("TaskDetailsDialog.title")); // NOI18N
        setName("Form"); // NOI18N

        descriptionLabel.setDisplayedMnemonic('D');
        descriptionLabel.setLabelFor(descriptionField);
        ResourceBundle bundle1 = ResourceBundle.getBundle("com/myco/todo/view/Bundle"); // NOI18N
        descriptionLabel.setText(bundle1.getString("TaskDetailsDialog.descriptionLabel.text")); // NOI18N
        descriptionLabel.setName("descriptionLabel"); // NOI18N

        priorityLabel.setDisplayedMnemonic('P');
        priorityLabel.setText(bundle1.getString("TaskDetailsDialog.priorityLabel.text")); // NOI18N
        priorityLabel.setName("priorityLabel"); // NOI18N

        descriptionField.setText(bundle1.getString("TaskDetailsDialog.descriptionField.text")); // NOI18N
        descriptionField.setName("descriptionField"); // NOI18N
        descriptionField.addKeyListener(formListener);

        priorityField.setMajorTickSpacing(1);
        priorityField.setMaximum(3);
        priorityField.setMinimum(1);
        priorityField.setPaintLabels(true);
        priorityField.setPaintTicks(true);
        priorityField.setSnapToTicks(true);
        priorityField.setValue(2);
        priorityField.setName("priorityField"); // NOI18N
        priorityField.addChangeListener(formListener);

        dueDateLabel.setDisplayedMnemonic('u');
        dueDateLabel.setText(bundle1.getString("TaskDetailsDialog.dueDateLabel.text")); // NOI18N
        dueDateLabel.setName("dueDateLabel"); // NOI18N

        dueDateField.setName("dueDateField"); // NOI18N
        URL iconUrl = getClass().getResource("/com/myco/todo/icons/sm/calendar-day.png");
        Image calImg = Toolkit.getDefaultToolkit().getImage(iconUrl);
        ImageIcon calendar = new ImageIcon(calImg);
        JButton btn = dueDateField.getComponentToggleCalendarButton();
        btn.setText("");
        btn.setIcon(calendar);
        dueDateField.addPropertyChangeListener(formListener);

        showAlertField.setMnemonic('A');
        showAlertField.setText(bundle1.getString("TaskDetailsDialog.showAlertField.text")); // NOI18N
        showAlertField.setName("showAlertField"); // NOI18N
        showAlertField.addChangeListener(formListener);
        showAlertField.addActionListener(formListener);

        daysBeforeField.setModel(new SpinnerNumberModel(0, 0, null, 1));
        daysBeforeField.setEnabled(false);
        daysBeforeField.setName("daysBeforeField"); // NOI18N
        daysBeforeField.addChangeListener(formListener);

        daysBeforeLabel.setText(bundle1.getString("TaskDetailsDialog.daysBeforeLabel.text")); // NOI18N
        daysBeforeLabel.setEnabled(false);
        daysBeforeLabel.setName("daysBeforeLabel"); // NOI18N

        obsLabel.setDisplayedMnemonic('O');
        obsLabel.setText(bundle1.getString("TaskDetailsDialog.obsLabel.text")); // NOI18N
        obsLabel.setName("obsLabel"); // NOI18N

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        obsField.setColumns(20);
        obsField.setLineWrap(true);
        obsField.setRows(5);
        obsField.setWrapStyleWord(true);
        obsField.setName("obsField"); // NOI18N
        obsField.addKeyListener(formListener);
        jScrollPane1.setViewportView(obsField);

        jSeparator1.setName("jSeparator1"); // NOI18N

        problemLabel.setFont(new Font("Bitstream Vera Sans", 1, 12)); // NOI18N
        problemLabel.setForeground(new Color(204, 0, 0));
        problemLabel.setText(bundle1.getString("TaskDetailsDialog.problemLabel.text")); // NOI18N
        problemLabel.setMaximumSize(new Dimension(23456789, 15));
        problemLabel.setMinimumSize(new Dimension(0, 15));
        problemLabel.setName("problemLabel"); // NOI18N
        problemLabel.setPreferredSize(new Dimension(0, 15));

        removeButton.setIcon(new ImageIcon(getClass().getResource("/com/myco/todo/icons/sm/delete.png"))); // NOI18N
        removeButton.setMnemonic('R');
        removeButton.setText(bundle1.getString("TaskDetailsDialog.removeButton.text")); // NOI18N
        removeButton.setActionCommand(bundle1.getString("TaskDetailsDialog.removeButton.actionCommand")); // NOI18N
        removeButton.setName("removeButton"); // NOI18N
        removeButton.addActionListener(actionSupport);

        cancelButton.setIcon(new ImageIcon(getClass().getResource("/com/myco/todo/icons/sm/cancel.png"))); // NOI18N
        cancelButton.setMnemonic('n');
        cancelButton.setText(bundle1.getString("TaskDetailsDialog.cancelButton.text")); // NOI18N
        cancelButton.setName("cancelButton"); // NOI18N
        cancelButton.addActionListener(formListener);

        saveButton.setIcon(new ImageIcon(getClass().getResource("/com/myco/todo/icons/sm/accept.png"))); // NOI18N
        saveButton.setMnemonic('S');
        saveButton.setText(bundle1.getString("TaskDetailsDialog.saveButton.text")); // NOI18N
        saveButton.setActionCommand(bundle1.getString("TaskDetailsDialog.saveButton.actionCommand")); // NOI18N
        saveButton.setEnabled(false);
        saveButton.setName("saveButton"); // NOI18N
        saveButton.addActionListener(actionSupport);
        saveButton.addActionListener(formListener);

        completedField.setMnemonic('C');
        completedField.setText(bundle1.getString("TaskDetailsDialog.completedField.text")); // NOI18N
        completedField.setName("completedField"); // NOI18N
        completedField.addChangeListener(formListener);

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1, GroupLayout.Alignment.TRAILING)
            .addComponent(problemLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                            .addComponent(obsLabel)
                            .addComponent(dueDateLabel)
                            .addComponent(descriptionLabel)
                            .addComponent(priorityLabel))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(descriptionField, GroupLayout.PREFERRED_SIZE, 342, GroupLayout.PREFERRED_SIZE)
                            .addComponent(priorityField, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(dueDateField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(showAlertField)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(daysBeforeField, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(daysBeforeLabel))
                            .addComponent(jScrollPane1)))
                    .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(completedField)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(saveButton)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cancelButton, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(removeButton, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        layout.linkSize(SwingConstants.HORIZONTAL, new Component[] {cancelButton, removeButton, saveButton});

        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(problemLabel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(descriptionLabel)
                    .addComponent(descriptionField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                    .addComponent(priorityLabel)
                    .addComponent(priorityField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(dueDateLabel)
                    .addComponent(dueDateField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(showAlertField)
                    .addComponent(daysBeforeField, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                    .addComponent(daysBeforeLabel))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(obsLabel)
                    .addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(removeButton)
                    .addComponent(cancelButton)
                    .addComponent(saveButton)
                    .addComponent(completedField))
                .addContainerGap())
        );

        pack();
    }

    // Code for dispatching events from components to event handlers.

    private class FormListener implements ActionListener, KeyListener, PropertyChangeListener, ChangeListener {
        FormListener() {}
        public void actionPerformed(ActionEvent evt) {
            if (evt.getSource() == cancelButton) {
                TaskDetailsDialog.this.cancelButtonActionPerformed(evt);
            }
            else if (evt.getSource() == showAlertField) {
                TaskDetailsDialog.this.showAlertFieldActionPerformed(evt);
            }
            else if (evt.getSource() == saveButton) {
                TaskDetailsDialog.this.saveButtonActionPerformed(evt);
            }
        }

        public void keyPressed(KeyEvent evt) {
        }

        public void keyReleased(KeyEvent evt) {
            if (evt.getSource() == descriptionField) {
                TaskDetailsDialog.this.descriptionFieldKeyReleased(evt);
            }
            else if (evt.getSource() == obsField) {
                TaskDetailsDialog.this.obsFieldKeyReleased(evt);
            }
        }

        public void keyTyped(KeyEvent evt) {
        }

        public void propertyChange(PropertyChangeEvent evt) {
            if (evt.getSource() == dueDateField) {
                TaskDetailsDialog.this.dueDateFieldPropertyChange(evt);
            }
        }

        public void stateChanged(ChangeEvent evt) {
            if (evt.getSource() == priorityField) {
                TaskDetailsDialog.this.priorityFieldStateChanged(evt);
            }
            else if (evt.getSource() == showAlertField) {
                TaskDetailsDialog.this.showAlertFieldStateChanged(evt);
            }
            else if (evt.getSource() == daysBeforeField) {
                TaskDetailsDialog.this.daysBeforeFieldStateChanged(evt);
            }
            else if (evt.getSource() == completedField) {
                TaskDetailsDialog.this.completedFieldStateChanged(evt);
            }
        }
    }// </editor-fold>//GEN-END:initComponents

    private void cancelButtonActionPerformed(ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        cancelled = true;
        setVisible(false);
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void showAlertFieldActionPerformed(ActionEvent evt) {//GEN-FIRST:event_showAlertFieldActionPerformed
        daysBeforeField.setEnabled(showAlertField.isSelected());
        daysBeforeLabel.setEnabled(showAlertField.isSelected());
    }//GEN-LAST:event_showAlertFieldActionPerformed

    private void descriptionFieldKeyReleased(KeyEvent evt) {//GEN-FIRST:event_descriptionFieldKeyReleased
        if (!loading) {
            task.setDescription(descriptionField.getText());
            validateInput();
        }
    }//GEN-LAST:event_descriptionFieldKeyReleased

    private void priorityFieldStateChanged(ChangeEvent evt) {//GEN-FIRST:event_priorityFieldStateChanged
        if (!loading) {
            task.setPriority(priorityField.getValue());
            validateInput();
        }
    }//GEN-LAST:event_priorityFieldStateChanged

    private void dueDateFieldPropertyChange(PropertyChangeEvent evt) {//GEN-FIRST:event_dueDateFieldPropertyChange
        if (!loading) {
            task.setDueDate(dueDateField.getDate());
            validateInput();
        }
    }//GEN-LAST:event_dueDateFieldPropertyChange

    private void showAlertFieldStateChanged(ChangeEvent evt) {//GEN-FIRST:event_showAlertFieldStateChanged
        if (!loading) {
            task.setAlert(showAlertField.isSelected());
            validateInput();
        }
    }//GEN-LAST:event_showAlertFieldStateChanged

    private void daysBeforeFieldStateChanged(ChangeEvent evt) {//GEN-FIRST:event_daysBeforeFieldStateChanged
        if (!loading) {
            task.setDaysBefore((int) daysBeforeField.getValue());
            validateInput();
        }
    }//GEN-LAST:event_daysBeforeFieldStateChanged

    private void obsFieldKeyReleased(KeyEvent evt) {//GEN-FIRST:event_obsFieldKeyReleased
        if (!loading) {
            task.setObs(obsField.getText());
            validateInput();
        }
    }//GEN-LAST:event_obsFieldKeyReleased

    private void completedFieldStateChanged(ChangeEvent evt) {//GEN-FIRST:event_completedFieldStateChanged
        if (!loading) {
            task.setCompleted(completedField.isSelected());
            validateInput();
        }
    }//GEN-LAST:event_completedFieldStateChanged

    private void saveButtonActionPerformed(ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        saveButton.setEnabled(false);
    }//GEN-LAST:event_saveButtonActionPerformed

    private void validateInput() {
        saveButton.setEnabled(!original.equals(task));
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JButton cancelButton;
    private JCheckBox completedField;
    private JSpinner daysBeforeField;
    private JLabel daysBeforeLabel;
    private JTextField descriptionField;
    private JLabel descriptionLabel;
    private DatePicker dueDateField;
    private JLabel dueDateLabel;
    private JScrollPane jScrollPane1;
    private JSeparator jSeparator1;
    private JTextArea obsField;
    private JLabel obsLabel;
    private JSlider priorityField;
    private JLabel priorityLabel;
    private JLabel problemLabel;
    private JButton removeButton;
    private JButton saveButton;
    private JCheckBox showAlertField;
    // End of variables declaration//GEN-END:variables
}
