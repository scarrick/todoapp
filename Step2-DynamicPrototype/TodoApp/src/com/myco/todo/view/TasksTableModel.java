/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   TodoApp
 *  Class      :   TasksTableModel.java
 *  Author     :   Sean Carrick
 *  Created    :   Feb 21, 2023
 *  Modified   :   Feb 21, 2023
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ============  ===================  =========================================
 *  Feb 21, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.myco.todo.view;

import com.myco.todo.model.Task;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class TasksTableModel extends AbstractTableModel {

    private static final long serialVersionUID = -4627555170710454481L;

    private final List<Task> tasks;
    private List<Task> filteredTasks;
    private boolean showCompleted = true;
    private boolean sortByPriority = true;
    private final DateTimeFormatter fmt = DateTimeFormatter.ISO_LOCAL_DATE;

    public TasksTableModel(List<Task> tasks) {
        this.tasks = tasks;
        filterTasks();
    }

    @Override
    public int getRowCount() {
        return filteredTasks.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Task task = filteredTasks.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return task.getPriority();
            case 1:
                return task.getDescription();
            case 2:
                return task.isAlert();
            case 3:
                return task.getDueDate();
            default:
                return null;
        }
    }

    public Task getSelectedTask(int rowIndex) {
        if (rowIndex > filteredTasks.size()) {
            return null;
        }
        return filteredTasks.get(rowIndex);
    }

    public boolean isShowCompleted() {
        return showCompleted;
    }

    public void setShowCompleted(boolean showCompleted) {
        this.showCompleted = showCompleted;
        filterTasks();
    }

    public boolean isSortByPriority() {
        return sortByPriority;
    }

    public void setSortByPriority(boolean sortByPriority) {
        this.sortByPriority = sortByPriority;
        filterTasks();
    }

    private void filterTasks() {
        filteredTasks = new ArrayList<>();

        for (Task task : tasks) {
            if (!isShowCompleted() && task.isCompleted()) {
                continue;
            }
            filteredTasks.add(task);
        }

        if (!isSortByPriority()) {
            Collections.sort(filteredTasks, new Comparator<Task>() {
                @Override
                public int compare(Task o1, Task o2) {
                    if (o1.getDueDate() == null) {
                        return 1;
                    } else if (o2.getDueDate() == null) {
                        return -1;
                    } else if (o1.getDueDate().equals(o2.getDueDate())) {
                        if (o1.getPriority() == o2.getPriority()) {
                            return o1.getDescription().compareTo(o2.getDescription());
                        } else {
                            return o1.getPriority() > o2.getPriority() ? 1 : -1;
                        }
                    } else {
                        return o1.getDueDate().compareTo(o2.getDueDate());
                    }
                }

            });
            fireTableDataChanged();
        }
    }

}
