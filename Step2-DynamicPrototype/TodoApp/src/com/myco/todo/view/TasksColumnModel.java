/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   TodoApp
 *  Class      :   TasksColumnModel.java
 *  Author     :   Sean Carrick
 *  Created    :   Feb 22, 2023
 *  Modified   :   Feb 22, 2023
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ============  ===================  =========================================
 *  Feb 22, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.myco.todo.view;

import java.awt.FontMetrics;
import java.util.ResourceBundle;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableColumn;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public final class TasksColumnModel extends DefaultTableColumnModel {

    private static final long serialVersionUID = 5327798854337442375L;

    private static final String COL0
            = ResourceBundle.getBundle("com/myco/todo/view/Bundle")
                    .getString("TasksColumnModel.col0");
    private static final String COL1
            = ResourceBundle.getBundle("com/myco/todo/view/Bundle")
                    .getString("TasksColumnModel.col1");
    private static final String COL2
            = ResourceBundle.getBundle("com/myco/todo/view/Bundle")
                    .getString("TasksColumnModel.col2");
    private static final String COL3
            = ResourceBundle.getBundle("com/myco/todo/view/Bundle")
                    .getString("TasksColumnModel.col3");

    public TasksColumnModel(FontMetrics fm) {
        int digit = fm.stringWidth("0");
        int alpha = fm.stringWidth("W");
        addColumn(createColumn(0, 3 * digit, fm, false, COL0));
        addColumn(createColumn(1, 20 * alpha, fm, true, COL1));
        addColumn(createColumn(2, 3 * alpha, fm, false, COL2));
        addColumn(createColumn(3, 12 * digit, fm, false, COL3));
    }

    private TableColumn createColumn(int columnIndex, int width, FontMetrics fm,
            boolean resizable, String text) {
        int textWidth = fm.stringWidth(text + "  ");
        if (width < textWidth) {
            width = textWidth;
        }
        TableColumn col = new TableColumn(columnIndex);
        col.setCellRenderer(new TaskCellRenderer());
        col.setHeaderRenderer(null);
        col.setHeaderValue(text);
        col.setPreferredWidth(width);
        if (!resizable) {
            col.setMaxWidth(width);
            col.setMinWidth(width);
        }
        col.setResizable(resizable);
        return col;
    }

}
