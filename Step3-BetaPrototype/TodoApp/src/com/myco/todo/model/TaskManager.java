/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   TodoApp
 *  Class      :   TaskManager.java
 *  Author     :   Sean Carrick
 *  Created    :   Feb 22, 2023
 *  Modified   :   Feb 22, 2023
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ============  ===================  =========================================
 *  Feb 22, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.myco.todo.model;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class TaskManager {

    private final Parameters params;
    private Connection conn;
    private Statement stmt;
    private ResultSet rs;

    public TaskManager(Parameters params) throws DatabaseException {
        this.params = params;
        connect();
    }

    public void disconnect() {
        try {
            if (conn != null) {
                conn.commit();
                conn.close();
            }
            conn = null;    // For GC
            System.gc();
        } catch (SQLException ignore) {
            // We don't handle this...it is what it is.
        }
    }

    public void reconnect(String database) throws DatabaseException {
        disconnect();
        params.setDatabase(database);
        connect();
    }

    private void connect() throws DatabaseException {
        try {
            String url = params.getJdbcUrl();
            Class.forName(params.getJdbcDriver());
            conn = DriverManager.getConnection(url, "sa", "");
            if (!checkTables()) {
                createTables();
            }
        } catch (DatabaseException dbe) {
            throw new DatabaseException("Cannot initialize the database tables",
                    dbe.getCausedBy(), dbe.getCause());
        } catch (ClassNotFoundException cnfe) {
            throw new DatabaseException("Cannot load database driver: "
                    + cnfe.getMessage(), params.getJdbcDriver(), cnfe);
        } catch (SQLException sqle) {
            throw new DatabaseException("Cannot open the database: "
                    + sqle.getMessage(),
                    sqle.getSQLState() + " (Error Code: " + sqle.getErrorCode()
                    + ")", sqle);
        }
    }

    private boolean checkTables() {
        try {
            String sql = "SELECT COUNT(*) FROM todo";
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            return rs.isBeforeFirst();
        } catch (SQLException e) {
            return false;
        } finally {
            cleanup();
        }
    }

    private void createTables() throws DatabaseException {
        StringBuilder sb = new StringBuilder();

        sb.append("CREATE TABLE todo (");
        // For auto-generated identities in MySQL databases:
        //sb.append("id BIGINT NOT NULL AUTONUMBER PRIMARY KEY");
        // For auto-generated identities in Apache Derby databases:
        //sb.append("id BIGINT NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1");
        sb.append("id IDENTITY, "); // HSQLDB auto-generated identity
        sb.append("description VARCHAR(50) NOT NULL, ");
        sb.append("priority INTEGER, ");
        sb.append("completed BOOLEAN, ");
        sb.append("dueDate DATE, ");
        sb.append("alert BOOLEAN, ");
        sb.append("daysBefore INTEGER, ");
        sb.append("obs VARCHAR(500)");
        sb.append(")");

        update(sb.toString());
    }

    private void cleanup() {
        try {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }

            rs = null;
            stmt = null;
            System.gc();
        } catch (SQLException ignore) {
            // We don't care...
        }
    }

    private void update(String sql) throws DatabaseException {
        try {
            stmt = conn.createStatement();
            int affected = stmt.executeUpdate(sql);
            System.out.println("[UPDATE] " + affected + " rows affected.");
        } catch (SQLException ex) {
            throw new DatabaseException("Cannot modify the database: "
                    + ex.getMessage(), sql, ex);
        } finally {
            cleanup();
        }
    }

    private PreparedStatement prepare(String sql) throws SQLException {
        try {
            return conn.prepareStatement(sql);
        } finally {
            cleanup();
        }
    }

    private List<Task> query(String where, String orderBy) throws DatabaseException {
        List<Task> results = new ArrayList<>();
        String sql = "SELECT id, description, priority, completed, dueDate, "
                + "alert, daysBefore, obs FROM todo";
        if (where != null) {
            sql += " WHERE " + where;
        }
        if (orderBy != null) {
            sql += " ORDER BY " + orderBy;
        }
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Task task = new Task();
                task.setId(rs.getInt(1));
                task.setDescription(rs.getString(2));
                task.setPriority(rs.getInt(3));
                task.setCompleted(rs.getBoolean(4));
                task.setDueDate(rs.getDate(5).toLocalDate());
                task.setAlert(rs.getBoolean(6));
                task.setDaysBefore(rs.getInt(7));
                task.setObs(rs.getString(8));
                results.add(task);
            }
        } catch (SQLException ex) {
            throw new DatabaseException("Cannot fetch from database: "
                    + ex.getMessage(), sql, ex);
        } finally {
            cleanup();
        }

        return results;
    }

    public List<Task> listAllTasks() throws DatabaseException {
        return query(null, null);
    }

    public List<Task> listAllTasks(boolean priorityOrDate) throws DatabaseException {
        return query(null, priorityOrDate
                ? "priority, dueDate, description"
                : "dueDate, priority, description");
    }

    public List<Task> listTasksWithAlert() throws DatabaseException {
        return query("alert = true AND "
                + "datediff('dd', curtime(), dueDate) <= daysBefore",
                "dueDate, priority, description");
    }

    public void addTask(Task task) throws ValidationException, DatabaseException {
        validate(task);
        String sql = "INSERT INTO todo (description, priority, completed, dueDate, "
                + "alert, daysBefore, obs) VALUES(?, ?, ?, ?, ?, ?, ?)";
        modify(sql, task);
    }

    public void updateTask(Task task) throws ValidationException, DatabaseException {
        validate(task);
        String msg = "UPDATE todo SET description = ?, priority = ?, "
                + "completed = ?, dueDate = ?, alert = ?, daysBefore = ?, "
                + "obs = ? WHERE id = " + task.getId();
        modify(msg, task);
    }

    private void validate(Task task) throws ValidationException {
        if (task.getDescription().trim().isEmpty()) {
            throw new ValidationException("A task description must be specified.");
        }
    }

    private void modify(String sql, Task task) throws DatabaseException {
        try {
            PreparedStatement pst = conn.prepareStatement(sql);
            stmt = pst;
            pst.setString(1, task.getDescription());
            pst.setInt(2, task.getPriority());
            pst.setBoolean(3, task.isCompleted());
            if (task.getDueDate() == null) {
                pst.setDate(4, null);
            } else {
                pst.setDate(4, Date.valueOf(task.getDueDate()));
            }
            pst.setBoolean(5, task.isAlert());
            pst.setInt(6, task.getDaysBefore());
            pst.setString(7, task.getObs());
            int affected = pst.executeUpdate();
            System.out.println("[UPDATE] " + affected + " rows affected.");
        } catch (SQLException ex) {
            throw new DatabaseException("Cannot update the database: "
                    + ex.getMessage(), sql, ex);
        } finally {
            cleanup();
        }
    }

    public void markAsCompleted(int id, boolean completed) throws DatabaseException {
        update("UPDATE todo SET completed = " + completed + " WHERE id = " + id);
    }

    public void removeTask(int id) throws DatabaseException {
        update("DELETE FROM todo WHERE id = " + id);
    }

}
