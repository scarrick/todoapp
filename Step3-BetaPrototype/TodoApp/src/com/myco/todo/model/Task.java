/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   TodoApp
 *  Class      :   Task.java
 *  Author     :   Sean Carrick
 *  Created    :   Feb 21, 2023
 *  Modified   :   Feb 21, 2023
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ============  ===================  =========================================
 *  Feb 21, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.myco.todo.model;

import java.io.Serializable;
import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Objects;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public final class Task implements Serializable, Cloneable {

    private static final long serialVersionUID = 607624362422825617L;

    private int id;
    private String description;
    private int priority;
    private LocalDate dueDate;
    private boolean alert;
    private int daysBefore;
    private String obs;
    private boolean completed;

    private LocalDate getTodayDate() {
        Calendar now = Calendar.getInstance();
        now.set(Calendar.HOUR_OF_DAY, 0);
        now.set(Calendar.MINUTE, 0);
        now.set(Calendar.SECOND, 0);
        now.set(Calendar.MILLISECOND, 0);
        LocalDate today = LocalDate.of(
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH) + 1,
                now.get(Calendar.DAY_OF_MONTH));
        return today;
    }

    public boolean isLate() {
        if (getDueDate() == null) {
            return false;
        }
        return getDueDate().isBefore(getTodayDate());
    }

    public boolean hasAlert() {
        if (!isAlert() || getDueDate() == null) {
            return false;
        }
        long days = Duration.between(getDueDate().atStartOfDay(),
                getTodayDate().atStartOfDay()).toDays();
        return days <= getDaysBefore();
    }

    public Task() {
        setCompleted(false);
        setAlert(false);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public boolean isAlert() {
        return alert;
    }

    public void setAlert(boolean alert) {
        this.alert = alert;
    }

    public int getDaysBefore() {
        return daysBefore;
    }

    public void setDaysBefore(int daysBefore) {
        this.daysBefore = daysBefore;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public Task getClone() {
        try {
            return (Task) clone();
        } catch (CloneNotSupportedException ex) {
            System.err.println(ex);
            System.err.println(ex.getLocalizedMessage());
            ex.printStackTrace(System.err);
            return null;
        }
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        super.clone();
        Task clone = new Task();
        clone.setAlert(hasAlert());
        clone.setCompleted(isCompleted());
        clone.setDaysBefore(getDaysBefore());
        clone.setDescription(getDescription());
        clone.setDueDate(getDueDate());
        clone.setId(getId());
        clone.setObs(getObs());
        clone.setPriority(getPriority());
        return clone;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + this.id;
        hash = 37 * hash + Objects.hashCode(this.description);
        hash = 37 * hash + this.priority;
        hash = 37 * hash + Objects.hashCode(this.dueDate);
        hash = 37 * hash + this.daysBefore;
        hash = 37 * hash + (this.completed ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Task other = (Task) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.priority != other.priority) {
            return false;
        }
        if (this.daysBefore != other.daysBefore) {
            return false;
        }
        if (this.completed != other.completed) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        return Objects.equals(this.dueDate, other.dueDate);
    }

    @Override
    public String toString() {
        DateTimeFormatter fmt = DateTimeFormatter.ISO_LOCAL_DATE;
        StringBuilder sb = new StringBuilder();

        // For a description < 20 characters:
        //+
        //+    Take care of this (Due on 2023-04-01) [P1]
        //+
        //+ For a description > 20 characters:
        //+
        //+    This needs to be ... (Due on 2023-04-01) [P1]
        if (description.length() > 20) {
            sb.append(description.substring(0, 17)).append("...");
        } else {
            sb.append(description);
        }
        sb.append(" (Due on ").append(fmt.format(dueDate)).append(") [P");
        sb.append(priority).append("]");

        return sb.toString();
    }

}
