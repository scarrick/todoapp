/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   TodoApp
 *  Class      :   QueryEditTasks.java
 *  Author     :   Sean Carrick
 *  Created    :   Feb 22, 2023
 *  Modified   :   Feb 22, 2023
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ============  ===================  =========================================
 *  Feb 22, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.myco.todo.controller;

import com.myco.todo.model.DatabaseException;
import com.myco.todo.model.ModelException;
import com.myco.todo.model.Task;
import com.myco.todo.model.TaskManager;
import com.myco.todo.view.TaskDetailsDialog;
import com.myco.todo.view.TasksWindow;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.format.DateTimeFormatter;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public final class QueryEditTasks implements ActionListener {

    private TasksWindow view;
    private TaskManager model;
    private TaskDetailsDialog details;

    public QueryEditTasks(TasksWindow view, TaskManager model) {
        this.view = view;
        this.model = model;
        setupActionListener();
        try {
            listTasks();
        } catch (DatabaseException ex) {
            view.setStatus(ex.getMessage(), true);
        }
    }

    private void setupActionListener() {
        view.addActionListener(this);
    }

    public void listTasks() throws DatabaseException {
        view.setTaskList(model.listAllTasks(true));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        view.setCursor(new Cursor(Cursor.WAIT_CURSOR));

        try {
            switch (e.getActionCommand()) {
                case "newTask":
                    editTask(true);
                    break;
                case "editTask":
                    editTask(false);
                    break;
                case "saveTask":
                    saveTask();
                    break;
                case "markTask":
                    markTasks();
                    break;
                case "removeTask":
                    removeTasks();
                    break;
                case "showAlerts":
                    showAlerts();
                    break;
                default:
                    break;
            }
        } catch (Exception ex) {
            view.setStatus(ex.getMessage(), true);
        }

        view.setCursor(null);
    }

    private void editTask(boolean newTask) {
        details = new TaskDetailsDialog(view, true);
        details.setNewTask(newTask);
        if (newTask) {
            details.setTask(new Task());
        } else {
            details.setTask(view.getSelectedTask());
        }
        details.addActionListener(this);
        details.setLocationRelativeTo(view);
        details.setVisible(true);
    }

    private void saveTask() {
        Task task = details.getTask();
        try {
            if (details.isNewTask()) {
                model.addTask(task);
            } else {
                model.updateTask(task);
            }
            details.dispose();
            details = null; // For GC
            listTasks();
        } catch (ModelException ex) {
            view.setStatus(ex.getMessage(), true);
        }
    }

    private void markTasks() {
        Task[] tasks = view.getSelectedTasks();
        try {
            for (Task task : tasks) {
                model.markAsCompleted(task.getId(), !task.isCompleted());
            }
        } catch (DatabaseException ex) {
            view.setStatus(ex.getMessage(), true);
        }
    }

    private void removeTasks() {
        Task[] tasks = view.getSelectedTasks();
        int removedCount = 0;
        try {
            for (Task task : tasks) {
                int response = JOptionPane.showConfirmDialog(
                        view,
                        "Removing a task cannot be undone.\n"
                        + "Are you sure you want to remove:\n\n"
                        + task.getDescription(),
                        "Remove Task",
                        JOptionPane.YES_NO_CANCEL_OPTION);
                if (response == JOptionPane.CANCEL_OPTION) {
                    break;
                } else if (response == JOptionPane.YES_OPTION) {
                    model.removeTask(task.getId());
                    removedCount++;
                }
            }
            if (removedCount > 0) {
                listTasks();
                if (details.isDisplayable()) {
                    details.dispose();
                }
            }
            view.setStatus("Removed " + removedCount + " out of a total of "
                    + tasks.length, false);
        } catch (ModelException ex) {
            view.setStatus(ex.getMessage(), true);
        }
    }

    public void showAlerts() {
        try {
            List<Task> tasks = model.listTasksWithAlert();
            DateTimeFormatter fmt = DateTimeFormatter.ISO_LOCAL_DATE;
            if (tasks.isEmpty()) {
                view.setStatus("There are no task alerts for today", false);
            } else {
                view.setStatus("There " + ((tasks.size() == 1) ? "is " : "are ")
                        + tasks.size() + ((tasks.size() == 1) ? " alert " : " alerts ")
                        + "for today.", false);
            }
            for (Task task : tasks) {
                JOptionPane.showMessageDialog(
                        view,
                        "This task has less than " + task.getDaysBefore()
                        + " days left:\n[" + task.getDescription() + "]\n"
                        + "The due date is " + fmt.format(task.getDueDate()),
                        "Alert",
                        JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (ModelException ex) {
            view.setStatus(ex.getMessage(), true);
        }
    }

}
